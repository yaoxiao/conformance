// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P2</tags>
/// <summary>
/// array/array_view of short_view_type
/// </summary>

// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out

#include <amp.h>

using namespace concurrency;

template<typename T>
void test_container() restrict(amp)
{
    T local_array;
    array_view<T, 1> arr_view(1, &local_array);
}


template<typename T>
void test_container() restrict(cpu)
{
    array<T, 1> arr(1);
}

int test() restrict(cpu,amp)
{
    test_container<cl_int2>();
    test_container<cl_int4>();
    test_container<cl_uint2>();
    test_container<cl_uint4>();
    
    test_container<cl_float2>();
    test_container<cl_float4>();
    
    //compilation should succeed
    return 0;
}

int main()
{
	 int result = 1;
    result &= test();
	return result;
}
