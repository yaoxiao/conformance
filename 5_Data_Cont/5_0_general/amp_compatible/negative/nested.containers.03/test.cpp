// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P0</tags>
/// <summary>test for nested array /array_view in array. test with const</summary>

// RUN: %clang_cc1 -std=c++amp -fsyntax-only %ampneg -verify %s

#include <amp.h>

using std::vector;
using namespace Concurrency;

int main()
{ 
    const array<const array<int>, 1> a(16);
    array<array_view<const int, 2>, 1> b(16);
    
    struct Type1
    {
       array<float> a;
    };
    const array<Type1, 1> c(16); // expected-error{{Compilation error expected at this line}}
    
    struct Type2
    {
       const array<float, 3> &a;
    };
    array<Type2, 1> d(16); // expected-error{{Compilation error expected at this line}}
    
    struct Type3
    {
       array_view<const float, 2> a;
    };
    array<Type3, 1> e(16);
    
    struct Type4
    {
       array_view<const float> a;
       array_view<double> b;
    };
    const array<Type4, 1> f(16);
    return 1;
}

//#Expects: Error: error C2973
//#Expects: Error: test.cpp\(19\)
//#Expects: Error: error C2973
//#Expects: Error: test.cpp\(20\)
//#Expects: Error: error C2973
//#Expects: Error: test.cpp\(26\)
//#Expects: Error: error C2973
//#Expects: Error: test.cpp\(32\)
//#Expects: Error: error C2973
//#Expects: Error: test.cpp\(38\)
//#Expects: Error: error C2973
//#Expects: Error: test.cpp\(45\)