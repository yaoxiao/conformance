// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>create array with static member which is an amp container</summary>
//#Expects: Error: error C2338
//#Expects: Error: error C2338
//
//#Expects: Error: test.cpp\(31\)
//#Expects: Error: test.cpp\(32\)
//
// RUN: %clang_cc1 -std=c++amp -fsyntax-only %ampneg -verify %s

#include <amp.h>
#include <vector>

using namespace concurrency;

struct A3
{
    static texture<float, 1> **arr;
};

int main()
{   
    const array<A3, 1> arr(10); // expected-error{{Compilation error expected at this line}}
    array_view<const A3, 1> arr_view(arr);

    return 1;
}

