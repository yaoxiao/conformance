//--------------------------------------------------------------------------------------
// File: helper.h
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//--------------------------------------------------------------------------------------
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
// file except in compliance with the License.  You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR
// CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
//
// See the Apache Version 2.0 License for specific language governing permissions
// and limitations under the License.
//
//--------------------------------------------------------------------------------------
//
//
// Helper functions to compare data in a vector to data in an array_view
// These are needed because the .data() member which serializes data out of 
// on array is not available on a 2D and 3D array_view

#pragma once

#include <amp.h>
#include <vector>

using namespace Concurrency;
using std::vector;

template<typename T>
bool compare(const vector<T>& vec, const array_view<T, 1>& av)
{
	for(int i = 0; i < av.get_extent()[0]; i++)
	{
		if(vec[i] != av(i))
		{
			return false;
		}
	}

	return true;
}

template<typename T>
bool compare(const vector<T>& vec, const array_view<T, 2>& av)
{
	for(int i = 0; i < av.get_extent()[1]; i++)
	{
		for(int j = 0; j < av.get_extent()[0]; j++)
		{
			if(vec[i * av.get_extent()[1] + j] != av(i,j))
			{
				return false;
			}
		}
	}

	return true;
}

template<typename T>
bool compare(const vector<T>& vec, const array_view<T, 3>& av)
{
	for(int i = 0; i < av.get_extent()[0]; i++)
	{
		for(int j = 0; j < av.get_extent()[1]; j++)
		{
			for(int k = 0; k < av.get_extent()[2]; k++)
			{
				if(vec[i * av.get_extent()[1] * av.get_extent()[2] + j * av.get_extent()[2] + k] != av(i,j,k))
				{
					return false;
				}
			}
		}
	}

	return true;
}

///<summary> Verifies that the extent of an array view matches expected values </summary>
template <typename TValue, int Rank>
bool verify_extent(concurrency::array_view<TValue, Rank> actual, concurrency::extent<Rank> expected)
{
	if(actual.get_extent() != expected)
	{
		return false;
	}

	return true;
}
