// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.

/// <summary>Test 'data' member function on array_views without a data source</summary>
// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out
#include <amp.h>
#include"../../../../../../device.h"
using namespace Concurrency;
using namespace Concurrency::Test;

/*
* Testing 'data' on array_view object created without data source.
* before p_f_e and after p_f_e
*/
int test1(const accelerator_view &av)
{
	int result;
	const int M = 256;
		
	array_view<int,1> arrViewSrc(M);
	int* dataPtr1 = arrViewSrc.data();
	
	result &= ( dataPtr1 != NULL );
	
	parallel_for_each(av,arrViewSrc.get_extent(),[=](index<1> idx) restrict(amp){
		arrViewSrc(idx) = idx[0];
	});
	
	int* dataPtr2 = arrViewSrc.data();
	result &= ( dataPtr2 != NULL);
	result &= ( dataPtr1 == dataPtr2);
	
	bool passed = true;
	for (size_t i = 0; i < M; ++i) {
		if (dataPtr2[i] != i) {
            //Log(LogType::Error) << "Expected = " << i << ", Actual = " << dataPtr2[i] << std::endl;
			passed = false;
		}
	}
	result &= passed;
	return result;
}

int main()
{
	accelerator_view av = require_device().get_default_view();
	int res;
	
	res &= test1(av);
	
	return res;
}

