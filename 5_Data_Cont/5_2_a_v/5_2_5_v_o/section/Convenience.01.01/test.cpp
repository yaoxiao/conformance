//--------------------------------------------------------------------------------------
// File: test.cpp
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//--------------------------------------------------------------------------------------
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
// file except in compliance with the License.  You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR
// CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
//
// See the Apache Version 2.0 License for specific language governing permissions
// and limitations under the License.
//
//--------------------------------------------------------------------------------------
//
/// <tags>P1</tags>
/// <summary>Create a section or a rank 1 array view using the convenience APIs on GPU</summary>

// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out
#include "../../../../array_view_test.h"

using namespace Concurrency;
using namespace Concurrency::Test;

int scalar_values()
{
    ArrayViewTest<int, 1> original(extent<1>(10));
    ArrayViewTest<int, 1> section = original.section(original.view().section(2, 5), index<1>(2));
    
	original.view()(2) = 13;
    original.set_known_value(index<1>(2), 13);
    
    // create a section on the GPU, use it to read and write
    auto gpu_original = original.view();
    parallel_for_each(extent<1>(1), [=](index<1>) restrict(amp) {
        array_view<int, 1> gpu_section = gpu_original.section(2, 5);
        gpu_section(3) = gpu_original(2);
    });

    section.set_known_value(index<1>(3), 13);
    
    return (original.view()(5) == 13 && section.view()(0) == 13) ? original.pass() : original.fail();
}

int only_index()
{
    ArrayViewTest<int, 1> original(extent<1>(10));
    ArrayViewTest<int, 1> section = original.section(original.view().section(index<1>(2)), index<1>(2));
    
	original.view()(2) = 13;
    original.set_known_value(index<1>(2), 13);
    
    // create a section on the GPU, use it to read and write
    auto gpu_original = original.view();
    parallel_for_each(extent<1>(1), [=](index<1>) restrict(amp) {
        array_view<int, 1> gpu_section = gpu_original.section(index<1>(2));
        gpu_section(3) = gpu_original(2);
    });

    section.set_known_value(index<1>(3), 13);
    
    return (original.view()(5) == 13 && section.view()(0) == 13) ? original.pass() : original.fail();
}

int only_extent()
{
    ArrayViewTest<int, 1> original(extent<1>(10));
    ArrayViewTest<int, 1> section = original.section(original.view().section(extent<1>(5)), index<1>(0));
    
	original.view()(2) = 13;
    original.set_known_value(index<1>(2), 13);
    
    // create a section on the GPU, use it to read and write
    auto gpu_original = original.view();
    parallel_for_each(extent<1>(1), [=](index<1>) restrict(amp) {
        array_view<int, 1> gpu_section = gpu_original.section(extent<1>(5));
        gpu_section(1) = gpu_original(2);
    });

    section.set_known_value(index<1>(1), 13);
    
    return (original.view()(1) == 13 && section.view()(2) == 13) ? original.pass() : original.fail();
}

int main()
{
    int res = 1;
	
    res &= scalar_values();
    res &= only_index();
    res &= only_extent();
	
    return !res;
}

