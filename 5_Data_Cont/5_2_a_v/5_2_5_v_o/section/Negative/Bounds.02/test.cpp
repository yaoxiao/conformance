//--------------------------------------------------------------------------------------
// File: test.cpp
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//--------------------------------------------------------------------------------------
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
// file except in compliance with the License.  You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR
// CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
//
// See the Apache Version 2.0 License for specific language governing permissions
// and limitations under the License.
//
//--------------------------------------------------------------------------------------
//
/// <tags>P1</tags>
/// <summary>Test a section that is too long</summary>

// RUN: %clang_cc1 -std=c++amp -fsyntax-only %ampneg -verify %s
#include <amp.h>

using namespace Concurrency;
using std::vector;

int main()
{
    std::vector<int> v(27);
    array_view<int, 3> av(extent<3>(3, 3, 3), v);
    try
    {
        array_view<int, 3> section = av.section(1, 1, 1, 2, 2, 3); // this should throw
        return 1;
    }
    catch (std::exception &re)
    {
        //Log(LogType::Info) << re.what() << std::endl;
        return 0;
    }
}
// expected-error@36 {{section that is too long}}
