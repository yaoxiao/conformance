//--------------------------------------------------------------------------------------
// File: test.cpp
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//--------------------------------------------------------------------------------------
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
// file except in compliance with the License.  You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR
// CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
//
// See the Apache Version 2.0 License for specific language governing permissions
// and limitations under the License.
//
//--------------------------------------------------------------------------------------
//
/// <tags>P1</tags>
/// <summary>Call reinterpret_as in a way that doesn't evenly divide length</summary>
// RUN: %clang_cc1 -std=c++amp -fsyntax-only %ampneg -verify %s
#include <amp.h>
#include <vector>
#include "../../../../../array_view_test.h"
#include "../../../../../coordinates.h"

using namespace Concurrency;
using namespace Concurrency::Test;

class Foo
{
    int r;
    int b;
    int g;
};

int main()
{
    std::vector<int> v(10);
    array_view<int, 1> av(10, v);
    try
    {
        // this should throw
        array_view<Foo, 1> r = av.reinterpret_as<Foo>();
        return 1;
    }
    catch (std::exception &re)
    {
        //Log() << re.what() << std::endl;
        return 0;
    }
}
// expected-error@35 {{reinterpret_as in a way that doesn't evenly divide length}}
