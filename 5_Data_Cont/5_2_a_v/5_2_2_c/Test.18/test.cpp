//--------------------------------------------------------------------------------------
// File: test.cpp
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//--------------------------------------------------------------------------------------
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
// file except in compliance with the License.  You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR
// CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
//
// See the Apache Version 2.0 License for specific language governing permissions
// and limitations under the License.
//
//--------------------------------------------------------------------------------------
//
/// <tags>P1</tags>
/// <summary>Test that we can create an array_view with data of class type (PODs only)</summary>
// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out
#include <amp.h>
#include <vector>
#include <algorithm>
#include "./../../../amp.compare.h"
#include "./../../../../device.h"
using namespace Concurrency;
using namespace Concurrency::Test;
using std::vector;

class class_a
{
public:
    int a_a;
    float a_b;
    unsigned int a_c;
};

class class_b : public class_a
{
public:
    int b_a;
    float b_b;
    unsigned int b_c;
};

int main()
{
    const int size = 100;

    vector<class_b> vec(size);
    for(int i = 0; i < size; i++)
    {
        vec[i].a_a = i + 1;
        vec[i].a_b = static_cast<float>(i + 2);
        vec[i].a_c = i + 3;
        vec[i].b_a = i + 4;
        vec[i].b_b = static_cast<float>(i + 5);
        vec[i].b_c = i + 6;
    }

    extent<1> ex(size);
    array<class_b, 1> arr(ex, vec.begin());
    array_view<class_b, 1> av(arr);

    if(arr.get_extent() != av.get_extent()) // verify extent
    {
        return 1;
    }

    accelerator device;
//    if (!get_device(Device::ALL_DEVICES, device))
//    {
//        return 2;
//    }
    accelerator_view acc_view = device.get_default_view();

    // use in parallel_for_each
    parallel_for_each(acc_view, av.get_extent(), [=] (index<1> idx) restrict(amp, cpu)
    {
        av[idx].a_a++;  av[idx].a_b++; av[idx].a_c++;
        av[idx].b_a++;  av[idx].b_b++; av[idx].b_c++;
    });

    // arr should automatically be updated at this point
    vec = arr;

    // verify data
    for(int i = 0; i < size; i++)
    {
        if(vec[i].a_a != (i+2) || av[i].a_a != (i+2))
        {
            return 1;
        }

        if(!AreAlmostEqual(vec[i].a_b, static_cast<float>(i+3)) || !AreAlmostEqual(av[i].a_b, static_cast<float>(i+3)))
        {
            return 1;
        }

        if(vec[i].a_c != static_cast<unsigned int>(i+4) || av[i].a_c != static_cast<unsigned int>(i+4))
        {
            return 1;
        }

        if(vec[i].b_a != (i+5) || av[i].b_a != (i+5))
        {
            return 1;
        }

        if(!AreAlmostEqual(vec[i].b_b, static_cast<float>(i+6)) || !AreAlmostEqual(av[i].b_b, static_cast<float>(i+6)))
        {
            return 1;
        }

        if(vec[i].b_c != static_cast<unsigned int>(i+7) || av[i].b_c != static_cast<unsigned int>(i+7))
        {
            return 1;
        }

    }

    return 0;
}

