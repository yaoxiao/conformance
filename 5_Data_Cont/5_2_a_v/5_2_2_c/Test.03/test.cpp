//--------------------------------------------------------------------------------------
// File: test.cpp
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//--------------------------------------------------------------------------------------
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
// file except in compliance with the License.  You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR
// CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
//
// See the Apache Version 2.0 License for specific language governing permissions
// and limitations under the License.
//
//--------------------------------------------------------------------------------------
//
/// <tags>P1</tags>
/// <summary>Test that a readonly array_view can be constructed from an AMP array in a CPU restricted function</summary>
// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out
#include "../../helper.h"
#include"../../../data.h"
#include"../../../../device.h"
#include <amp.h>
#include <vector>
#include <algorithm>

using namespace Concurrency;
using namespace Concurrency::Test;
using std::vector;

int main()
{
    const int size = 100;

    vector<int> vec(size);
    Fill<int>(vec.data(), size);

    extent<1> ex(size);
    accelerator device;
//    if (!get_device(Device::ALL_DEVICES, device))
//    {
//        return 2;
//    }
    accelerator_view acc_view = device.get_default_view();

    array<int, 1> arr(ex, vec.begin(), acc_view);
    
    array_view<int, 1> av1(arr);
    array_view<int, 1> av2(arr);

    if(arr.get_extent() != av1.get_extent()) // Verify extent
    {
        return 1;
    }

    // verify data
    if(!compare(vec, av1))
    {
         return 1;
    }
    
    // use in parallel_for_each
    parallel_for_each(arr.get_extent(), [=] (index<1> idx) restrict(amp,cpu)
    {
        av2[idx]++;
    });

    //verify that the data has been incremented by one
    for(unsigned int i = 0; i < static_cast<unsigned int>(vec.size()); i++)
    {
        if(av1[i] != vec[i] + 1)
        {
            return 1;
        }

        if(av2[i] != vec[i] + 1)
        {
            return 1;
        }
    }

    return 0;
}

