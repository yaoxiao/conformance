//--------------------------------------------------------------------------------------
// File: test.cpp
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//--------------------------------------------------------------------------------------
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
// file except in compliance with the License.  You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR
// CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
//
// See the Apache Version 2.0 License for specific language governing permissions
// and limitations under the License.
//
//--------------------------------------------------------------------------------------
//
/// <tags>P1</tags>
/// <summary>Create an array_view of const type using a single extent value, e0, and a container in a CPU restricted function</summary>
// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out
#include <amp.h>
#include <vector>
#include <algorithm>
#include "../../../../device.h"
using namespace Concurrency;
using namespace Concurrency::Test;
using std::vector;

int main()
{
    const int size = 100;

    vector<int> vec(size);
    for(int i = 0; i < size; i++) vec[i] = i;
    vector<int> vec_copy(vec);

    array_view<const int, 1> av1(size, vec);
    array_view<int, 1> av2(size, vec); // non const array to check updates to data in av1

    if(size != av1.get_extent()[0]) // Verify extent
    {
        return 1;
    }

    // verify data
    //if(!equal(vec.begin(), vec.end(), stdext::make_unchecked_array_iterator(av1.data())))
    //{
    //    return 1;
    //}

    accelerator device;
//    if (!get_device(Device::ALL_DEVICES, device))
//    {
//        return 2;
//    }
    accelerator_view acc_view = device.get_default_view();
    
    // use in parallel_for_each
    parallel_for_each(acc_view, av2.get_extent(), [=] (index<1> idx) restrict(amp,cpu)
    {
        av2[idx] = idx[0] + 1;
    });
    
    // verify data
    for(unsigned int i = 0; i < static_cast<unsigned int>(vec.size()); i++)
    {
        if(vec[i] != vec_copy[i] + 1)
        {
            return 1;
        }

        if(av1[i] != vec_copy[i] + 1)
        {
            return 1;
        }

        if(av2[i] != vec_copy[i] + 1)
        {
            return 1;
        }
    }

    return 0;
}

