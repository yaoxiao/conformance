// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.

/// <summary>Using array with CPU access type write on accelerator supporting zero-copy</summary>
// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out
#include "../Common.h"
#include "../../../../amp.compare.h"
#include "../../../../amp.data.h"
#include "../../../../../device.h"
using namespace Concurrency;
using namespace Concurrency::Test;
const int RANK = 3;
typedef int DATA_TYPE;
int main()
{		
	accelerator device = require_device_for<DATA_TYPE>();
	
	if(!device.get_supports_cpu_shared_memory())
	{
		return 2;
	}
	
	extent<RANK> arr_extent = CreateRandomExtent<RANK>(256);            
		array<DATA_TYPE, RANK> arr(arr_extent, device.get_default_view(), access_type_write);
	
	if(!VerifyCpuAccessType(arr, access_type_write)) { return 1; }
	
	Write<DATA_TYPE, RANK>(arr, 100);
	
	std::vector<DATA_TYPE> vec(arr.get_extent().size());
	copy(arr, vec.begin());
	
	if(VerifyAllSameValue(vec, static_cast<DATA_TYPE>(100)) != -1) { return 1; }
	
	for(int i = 1; i <= 100; i++)
	{
		parallel_for_each(device.get_default_view(), arr.get_extent(), [&](index<RANK> idx) restrict(amp)
		{
			arr[idx] += 2;
		});
		
		index<RANK> idx;
		for(int i = 0; i < RANK; i++) { idx[i] = arr.get_extent()[i] - 1; }
		
		arr[idx] = 50;			
	}
	
	vec.clear();
	copy(arr, vec.begin());
	
	if(vec[arr.get_extent().size() - 1] != 50)
	{
		return 1;
	}
	
	vec[arr.get_extent().size() - 1] = 300;
	
    return ((VerifyAllSameValue(vec, static_cast<DATA_TYPE>(300)) == -1));
}
