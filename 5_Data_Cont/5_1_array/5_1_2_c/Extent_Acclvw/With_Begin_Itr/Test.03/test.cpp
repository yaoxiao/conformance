// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED,
// INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>Create array using 1-D accelerator_view specialized constructors - uses deque</summary>
// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out
#include <deque>
#include "./../../../constructor.h"
#include <amp.h>

template<typename _type, int _D0>
bool test_feature()
{
    const int _rank = 1;

    std::deque<_type> data;
    for (int i = 0; i < _D0; i++)
        data.push_back((_type)rand());

    {
        bool pass = test_feature_itr<_type, _rank, _D0, accelerator_view>(data.begin(), data.end(), (_gpu_device).get_default_view()) &&
            test_feature_itr<_type, _rank, _D0, accelerator_view>(data.begin(), data.end(), (_gpu_device).create_view(queuing_mode_immediate)) &&
            test_feature_itr<_type, _rank, _D0, accelerator_view>(data.begin(), data.end(), (_gpu_device).create_view(queuing_mode_automatic)) &&
            test_feature_itr<_type, _rank, _D0, accelerator_view>(data.begin(), data.end(), (_gpu_device).create_view(queuing_mode_automatic));

        if (!pass)
            return false;
    }

    {
        bool pass = test_feature_itr<_type, _rank, _D0, accelerator_view>(data.begin(), (_gpu_device).get_default_view()) &&
            test_feature_itr<_type, _rank, _D0, accelerator_view>(data.begin(), (_gpu_device).create_view(queuing_mode_immediate)) &&
            test_feature_itr<_type, _rank, _D0, accelerator_view>(data.begin(), (_gpu_device).create_view(queuing_mode_automatic)) &&
            test_feature_itr<_type, _rank, _D0, accelerator_view>(data.begin(), (_gpu_device).create_view(queuing_mode_immediate));

        if (!pass)
            return false;
    }
    return true;
}

int main()
{
	int result = 1;

	result &= ((test_feature<int, 1>()));
	result &= ((test_feature<unsigned int, 91>()));
	result &= ((test_feature<float, 5>()));
	result &= ((test_feature<double, 31>()));
    
    return !result;
}
