// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED,
// INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>Create a array of nthe dimension with all extents 1</summary>
// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out
#include "./../../../constructor.h"
#include "./../../../../../../device.h"
#include <amp.h>

template<typename _type, int _rank>
bool test_feature()
{
	vector<accelerator> devices = accelerator::get_all();

	for (size_t i = 0; i < devices.size(); i++)
	{
		accelerator device = devices[i];

		{
			int edata[_rank];

			for (int i = 0; i < _rank; i++)
				edata[i] = 1;

			extent<_rank> e1(edata);
			array<_type, _rank> src(e1, device.get_default_view());

			// let the kernel initialize data;
			parallel_for_each(e1, [&](index<_rank> idx) restrict(amp)
			{
				src[idx] = _rank;
			});

			// Copy data to CPU
			vector<_type> opt(e1.size());
			opt = src;

			for (unsigned int i = 0; i < e1.size(); i++)
			{
				if (opt[i] != _rank)
					return false;
			}
		}
	}

	return true;
}

int main()
{
    int result = 1;

   
    result &= ((test_feature<int, 128>()));
    result &= ((test_feature<float, 128>()));
    
    return !result;
}

