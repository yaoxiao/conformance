// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED,
// INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>Construct array using accelerator_view specialized constructors - using default view.</summary>
// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out
#include "./../../../constructor.h"
#include <amp.h>

template<typename _type, int _rank>
int test_feature()
{
    vector<accelerator> devices = accelerator::get_all();

    for (size_t i = 0; i < devices.size(); i++)
    {
        if (devices[i] == _cpu_device)
            continue;

        accelerator device = devices[i];

        test_accl_constructor<_type, _rank, accelerator_view>(device.get_default_view());
    }

    return 0;
}


template<int _rank>
int test_feature()
{
    vector<accelerator> devices = accelerator::get_all();

    for (size_t i = 0; i < devices.size(); i++)
    {
        if (devices[i] == _cpu_device)
            continue;

        accelerator device = devices[i];
        
        if (!device.get_supports_double_precision())
        {
            return 2;
        }

        test_accl_constructor<double, _rank, accelerator_view>(device.get_default_view());
    }

    return 0;
}

int main()
{
	int result = 1;

	result &= ((test_feature<int, 5>() == 0));
	result &= ((test_feature<unsigned int, 5>() == 0));
	result &= ((test_feature<float, 5>() == 0));
    
	// test with double
	int dbl_result = (test_feature<5>() == 0);
    result &= dbl_result;
    
    return !result;
}

