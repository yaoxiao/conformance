// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED,
// INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>Verifies that the copy constructor will copy pending writes to data</summary>
// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out

#include <amp.h>
#include <vector>
#include <ostream>
#include "../../../../amp.compare.h"
#include "../../../../data.h"
using namespace Concurrency;
using namespace Concurrency::Test;
using std::vector;

int main()
{
    int result = 1;
    
    int size = 20;
    std::vector<float> src_v(size);
    Fill(src_v);
    
    array<float, 1> src_arr(size, src_v.begin());
    
    // create a new array and copy data 
    array<float> dest1(size);
    src_arr.copy_to(dest1);
    
    // now copy construct from the new copy of the data
    array<float> dest2(dest1);
    
    array_view<float> av(dest1);
    result &= (VerifyDataOnCpu(av, src_v));
    
    av = array_view<float>(dest2);
    result &= (VerifyDataOnCpu(av, src_v));

	return !result;
}
