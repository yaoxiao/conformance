// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED,
// INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>Construct array using accelerator and staging buffer specialized construtors - 2 arrays with different view of same device </summary>
// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out
#include <amp.h>
#include "./../../../../constructor.h"
#include "./../../../../../../../device.h"

template<typename _type, int _rank>
int test_feature()
{
	vector<accelerator> devices = accelerator::get_all(); // all but CPU devices

	int result = 1;
	for (size_t i = 0; i < devices.size(); i++)
	{
		accelerator& device = devices[i];

       result &= ((test_accl_staging_buffer_constructor<_type, _rank, accelerator_view>(device.get_default_view(), device.get_default_view())));
       result &= ((test_accl_staging_buffer_constructor<_type, _rank, accelerator_view>(device.create_view(queuing_mode_immediate), device.create_view(queuing_mode_automatic))));
       result &= ((test_accl_staging_buffer_constructor<_type, _rank, accelerator_view>(device.create_view(queuing_mode_automatic), device.create_view(queuing_mode_immediate))));
	}

	return result;
}

template<int _rank>
int test_feature()
{
	vector<accelerator> devices = accelerator::get_all();
	if(devices.size() == 0) {
		return 2;
	}

	int result = 1;
	for (size_t i = 0; i < devices.size(); i++)
	{
       accelerator& device = devices[i];

       result &= ((test_accl_staging_buffer_constructor<double, _rank, accelerator_view>(device.get_default_view(), device.get_default_view())));
       result &= ((test_accl_staging_buffer_constructor<double, _rank, accelerator_view>(device.create_view(queuing_mode_immediate), device.create_view(queuing_mode_automatic))));
       result &= ((test_accl_staging_buffer_constructor<double, _rank, accelerator_view>(device.create_view(queuing_mode_automatic), device.create_view(queuing_mode_immediate))));
	}

	return result;
}

int main()
{
	int result = 1;

	result &= ((test_feature<int, 5>()));
	result &= ((test_feature<unsigned int, 3>()));
	result &= ((test_feature<float, 4>()));
    
	// test with double
	int dbl_result = (test_feature<8>() > 0);
//	if(!dbl_result.get_is_skip()) // don't aggregate if skipped
		result &= dbl_result;
    
    return !result;
}

