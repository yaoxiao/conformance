// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED,
// INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>Construct array using accelerator_view and staging buffer specialized construtors - between  CPU and device</summary>
// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out
#include "./../../../../constructor.h"
#include <amp.h>

template<typename _type, int _rank>
int test_feature(accelerator *p_device_gpu,accelerator *p_device_cpu)
{
    if (p_device_cpu && p_device_gpu)
    { 
        if (!test_accl_staging_buffer_constructor<_type, _rank, accelerator_view>((*p_device_gpu).create_view(queuing_mode_automatic), (*p_device_cpu).create_view(queuing_mode_automatic)))
            return 1;
        if (!test_accl_staging_buffer_constructor<_type, _rank, accelerator_view>((*p_device_gpu).create_view(queuing_mode_immediate), (*p_device_cpu).create_view(queuing_mode_automatic)))
            return 1;
        if (!test_accl_staging_buffer_constructor<_type, _rank, accelerator_view>((*p_device_gpu).create_view(queuing_mode_automatic), (*p_device_cpu).create_view(queuing_mode_immediate)))
            return 1;
    }

	return 0;
}

int main()
{
    int result = 1;

    accelerator *p_device_cpu = NULL;
    accelerator *p_device_gpu = NULL;

    std::vector<accelerator> accl_devices = accelerator::get_all();

    for (size_t i = 0; i < accl_devices.size(); i ++)
    {
        //if ((accl_devices[i].get_device_path() == accelerator::cpu_accelerator) && (!p_device_cpu))
        //{
        //    p_device_cpu = &accl_devices[i];
        //}
        //else
        {
            p_device_gpu = &accl_devices[i];
        }
    }
    result &= ((test_feature<int, 5>(p_device_gpu,p_device_cpu)) == 0);
    result &= ((test_feature<unsigned int, 5>(p_device_gpu,p_device_cpu)) == 0);
    result &= ((test_feature<float, 5>(p_device_gpu,p_device_cpu)) == 0);
    
    return !result;
}

