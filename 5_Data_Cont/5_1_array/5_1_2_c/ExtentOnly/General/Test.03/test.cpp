// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED,
// INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>Create an array of a user defined structure</summary>
// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out
#include "./../../../constructor.h"
#include <amp.h>

typedef struct 
{
    int n;
    unsigned u;
    long l;
    unsigned long ul;
    float f; 
    double  d; 
} User_Defined;

template<typename _type, int _rank>
void kernel_userdefined(index<_rank>& idx, array<_type, _rank>& f) restrict(amp,cpu)
{
    f[idx].n = -5;
    f[idx].u = static_cast<unsigned>(-5);
    f[idx].l = -5;
    f[idx].ul = static_cast<unsigned long>(-5);
    f[idx].f = -5.0f;
    f[idx].d = -5.0;
}

template<typename _type, int _rank>
bool verify_kernel_userdefined(array<_type, _rank>& arr)
{
    std::vector<User_Defined> vdata = arr;
    for (size_t i = 0; i < vdata.size(); i++)
    {
        if ((vdata[i].n != -5) || (vdata[i].u != -5) || (vdata[i].l != -5) || (vdata[i].ul != -5) ||
            (vdata[i].f != -5.0f) || (vdata[i].d != -5.0))
            return false;
    }

    return true;
}

int main()
{
    // Test is using doubles therefore we have to make sure that it is not executed 
    // on devices that does not support double types.
    int result = 1;

    int extdata[] = {2, 2};
	 result &= ((test_array_userdefined<User_Defined, 1>(extdata)));
	 result &= ((test_array_userdefined<User_Defined, 2>(extdata)));

    return !result;
}
