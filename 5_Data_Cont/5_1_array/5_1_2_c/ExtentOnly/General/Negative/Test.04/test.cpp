// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED,
// INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>Create an array of unsupported non-integral types pointers</summary>
//#Expects: Error: error C3581
//#Expects: Error: error C3581
//#Expects: Error: error C3581
//#Expects: Error: error C2664
//#Expects: Error: error C2664
//#Expects: Error: error C2664
// RUN: %clang_cc1 -std=c++amp -fsyntax-only %ampneg -verify %s

#include "./../../../../constructor.h"
#include <amp.h>

int main()
{
    int extdata[] = {1};

    array<int*, 1> intPtr(extdata);// expected-error{{Compilation error expected at this line}}
    array<int**, 1> intPtrPtr(extdata);// expected-error{{Compilation error expected at this line}}
    array<float*, 1> floatPtr(extdata);// expected-error{{Compilation error expected at this line}}
    array<float**, 1> floatPtrPtr(extdata);// expected-error{{Compilation error expected at this line}}

	// We shouldn't compile
    return 1;
}

