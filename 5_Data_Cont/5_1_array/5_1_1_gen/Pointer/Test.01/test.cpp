// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED,
// INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>Using pointer to an array that is retrieved within the lambda is ok.</summary>
// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out
#include <amp.h>
#include "../../../../amp.compare.h"
#include "../../../../../device.h"

using namespace Concurrency;
using namespace Concurrency::Test;
int main() {
    accelerator_view av = require_device().get_default_view();

	// Setup input data
	int val = 1;
	array<int,1> ary(1, &val, av);

	// Setup output
	int expected_new_val = 2012;
	parallel_for_each(av, ary.get_extent(), [=,&ary](index<1> idx) restrict(amp) {
		// Get a pointer to our input container
		// This line verifies the expected type of the captured array, so no casting is neccessary
		array<int,1>* ary_ptr = &ary;

		(*ary_ptr)[idx] = expected_new_val;
	});

	// Copy the results to the CPU and verify
	return VerifyAllSameValue(ary, expected_new_val) == -1;
}

