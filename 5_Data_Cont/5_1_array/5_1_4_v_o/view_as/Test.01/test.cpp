// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED,
// INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>View an 1-D Array as 3D and do the updations in CPU</summary>

// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out

#include <amp.h>
#include <vector>
#include "./../../../../array_test.h"

using namespace Concurrency;
using namespace Concurrency::Test;

int main()
{
    extent<3> ex(4, 5, 3);
    ArrayTest<int, 1> original(extent<1>(ex.size()));
    
    extent_coordinate_nest<3> coordinates(ex);
    array_view<int, 3> rank3 = original.arr().view_as(ex);
    
    // read and write between the original and higher rank view
    index<3> set_original(2, 3, 1);
    index<1> set_original_linear(coordinates.get_linear(set_original));
    gpu_write<int,1>(original.arr(), set_original_linear, 17);
    original.set_known_value(set_original_linear, 17);
    
    index<3> set_viewAs(2,3,2);
    index<1> set_viewAs_linear(coordinates.get_linear(set_viewAs));
    rank3[set_viewAs] = rank3[set_original];
    original.set_known_value(set_viewAs_linear, 17);

    return
        gpu_read<int,1>(original.arr(), set_viewAs_linear) == 17 &&
        rank3[set_original] == 17
        ? 0 : 1;
}

