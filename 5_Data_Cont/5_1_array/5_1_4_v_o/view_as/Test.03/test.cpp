// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED,
// INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>View an 1D Array as a shorter 1D AV</summary>

// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out
#include <amp.h>
#include <vector>
#include "./../../../../array_test.h"
using namespace Concurrency;
using namespace Concurrency::Test;

int main()
{
    ArrayTest<int, 1> original(extent<1>(15));
    array<int, 1> &original_arr = original.arr();
    array_view<int, 1> shorter = original_arr.view_as(extent<1>(10));
    
    // read and write between the original and higher rank view
    index<1> set_original(9);
    gpu_write<int,1>(original_arr, set_original, 17);
    original.set_known_value(set_original, 17);
    
    index<1> set_view(8);
    shorter[set_view] = shorter[9];
    original.set_known_value(set_view, 17);
    
    return
        gpu_read<int,1>(original_arr, set_view) == 17 &&
        shorter[set_original] == 17
        ? 0 : 1;
}

