// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.

/// <summary>Copy from custom container to array using iterator which is strictly input iterator</summary>
// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out
#include <amp.h>
#include "../../CustomIterators.h"
#include "../../../../../amp.compare.h"

using namespace Concurrency;
using namespace Concurrency::Test;

int main()
{
	CustomIterator::CustomContainer<int> srcCont(10);
	std::fill(srcCont.container.begin(), srcCont.container.end(), 5);
	
	accelerator cpuDevice(accelerator::cpu_accelerator);
	array<int, 1> dataArray(10, cpuDevice.get_default_view());
	array_view<int, 1> destArrayView(dataArray);
	
	copy(srcCont.read_begin(), destArrayView);
	
	return VerifyDataOnCpu(destArrayView, srcCont.container, 0);
}

