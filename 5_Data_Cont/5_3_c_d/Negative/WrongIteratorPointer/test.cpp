// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.

/// <summary>Copying between standard container and destination with different types</summary>
// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl

#include <amp.h>

using namespace Concurrency;

int main()
{
    accelerator cpuDevice(accelerator::cpu_accelerator);
    
    std::vector<int> cont(3);
    std::fill(cont.begin(), cont.end(), 5);
    array<int, 1> destArray(3, cpuDevice.get_default_view());

    try
    {
        copy(cont.end(), cont.begin(), destArray);
    }
    catch(std::exception)
    {
        return 0;
    }

    return 1;
}

