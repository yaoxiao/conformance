// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.

/// <summary>Copy from Staging Array to Array View</summary>
// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out
#include <amp.h>
#include "../../CopyTestFlow.h"

using namespace Concurrency;
const int RANK = 3;
void print_access_type_tuple(std::tuple<access_type>& tup)
{
	//Log() << "CPU Access Types: (" << std::get<0>(tup) << ")" << std::endl;
}

class StagingArrayToArrayViewTests
{
private:
	accelerator cpu_acc;
	accelerator gpu_acc;
	
	std::vector<std::tuple<access_type>> access_types_vec;

public:
	StagingArrayToArrayViewTests()
	{
		cpu_acc = accelerator(accelerator::cpu_accelerator);
		gpu_acc = require_device_for<DATA_TYPE>();
		
		if(gpu_acc.get_supports_cpu_shared_memory())
		{
            //WLog() << "Accelerator " << gpu_acc.get_description() << " supports zero copy" << std::endl;
			
			// Set the default cpu access type for this accelerator
			gpu_acc.set_default_cpu_access_type(access_type_read_write);
						
			access_types_vec.push_back(std::make_tuple(access_type_none));
			access_types_vec.push_back(std::make_tuple(access_type_read));
			access_types_vec.push_back(std::make_tuple(access_type_write));
			access_types_vec.push_back(std::make_tuple(access_type_read_write));
		}
		else
		{
			access_types_vec.push_back(std::make_tuple(access_type_auto));
		}
	}

	int CpuAccViewToCpuAccView()
	{
		accelerator_view cpu_av = cpu_acc.get_default_view();
		accelerator_view arr_av = cpu_acc.get_default_view();
		accelerator_view stg_arr_av = gpu_acc.get_default_view();
		
		int res;
		
		for(auto a_t_tuple : access_types_vec)
		{
            //print_access_type_tuple(a_t_tuple);
			res &= CopyAndVerifyFromStagingArrayToArrayView<DATA_TYPE, RANK>(cpu_av, arr_av, stg_arr_av, std::get<0>(a_t_tuple));
		}
		
		return res;
	}

	int GpuAccViewToGpuAccView()
	{
		accelerator_view cpu_av = cpu_acc.get_default_view();
		accelerator_view arr_av = gpu_acc.get_default_view();
		accelerator_view stg_arr_av = gpu_acc.get_default_view();
		
		int res;
		
		for(auto a_t_tuple : access_types_vec)
		{
            //print_access_type_tuple(a_t_tuple);
			res &= CopyAndVerifyFromStagingArrayToArrayView<DATA_TYPE, RANK>(cpu_av, arr_av, stg_arr_av, std::get<0>(a_t_tuple));
		}
		
		return res;
	}

	int CpuAccView1ToCpuAccView2()
	{
		accelerator_view cpu_av = cpu_acc.get_default_view();
		accelerator_view arr_av = cpu_acc.create_view();
		accelerator_view stg_arr_av = gpu_acc.get_default_view();
		
		int res;
		
		for(auto a_t_tuple : access_types_vec)
		{
            //print_access_type_tuple(a_t_tuple);
			res &= CopyAndVerifyFromStagingArrayToArrayView<DATA_TYPE, RANK>(cpu_av, arr_av, stg_arr_av, std::get<0>(a_t_tuple));
		}
		
		return res;
	}

	int GpuAccView1ToGpuAccView2()
	{
		accelerator_view cpu_av = cpu_acc.get_default_view();
		accelerator_view arr_av = gpu_acc.create_view();
		accelerator_view stg_arr_av = gpu_acc.get_default_view();
		
		int res;
		
		for(auto a_t_tuple : access_types_vec)
		{
            //print_access_type_tuple(a_t_tuple);
			res &= CopyAndVerifyFromStagingArrayToArrayView<DATA_TYPE, RANK>(cpu_av, arr_av, stg_arr_av, std::get<0>(a_t_tuple));
		}
		
		return res;
	}
};

int main()
{	
	StagingArrayToArrayViewTests tests;	
	int res = 1;

    res &= tests.CpuAccViewToCpuAccView();
    res &= tests.GpuAccViewToGpuAccView();
    res &= tests.CpuAccView1ToCpuAccView2();
    res &= tests.GpuAccView1ToGpuAccView2();

	return !res;
}

