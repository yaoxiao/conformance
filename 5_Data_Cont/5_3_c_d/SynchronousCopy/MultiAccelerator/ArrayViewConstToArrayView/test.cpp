// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.

/// <summary>Copy from Array view with const type to Array View</summary>
// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out
#include "../../../CopyTestFlow.h"
#include <amp.h>

using namespace Concurrency;
const int RANK = 3;
class ArrayViewConstToArrayViewTests
{
private:
	accelerator cpu_acc;
	accelerator gpu_acc1;
	accelerator gpu_acc2;
	
	access_list access_types_vec;	

public:
	ArrayViewConstToArrayViewTests()
	{
		cpu_acc = accelerator(accelerator::cpu_accelerator);
		gpu_acc1 = require_device_for<DATA_TYPE>();
		gpu_acc2 = require_device_for<DATA_TYPE>(gpu_acc1);
		
		compute_access_type_list(access_types_vec, gpu_acc1, gpu_acc2, access_type_read_write1, access_type_read_write2);
	}

	int Gpu1AccViewToGpu2AccView()
	{
		accelerator_view gpu_av1 = gpu_acc1.get_default_view();
		accelerator_view gpu_av2 = gpu_acc2.get_default_view();
		
		int res = 1;
		
		for(auto a_t_tuple : access_types_vec)
		{
            //print_access_type_tuple(a_t_tuple);
			res &= CopyAndVerifyFromArrayViewConstToArrayView<DATA_TYPE, RANK>(gpu_av1, gpu_av2, std::get<0>(a_t_tuple), std::get<1>(a_t_tuple));
		}

		return res;
	}
};

int main()
{	
	ArrayViewConstToArrayViewTests tests;
	int res = 1;

	res &= tests.Gpu1AccViewToGpu2AccView();

	return !res;
}

