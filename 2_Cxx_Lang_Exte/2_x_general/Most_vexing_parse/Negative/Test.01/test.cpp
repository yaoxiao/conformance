// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P2</tags>
/// <summary>Negative test for the "most vexing parse" this is an object declaration</summary>
//#Expects: Error: C2146

//RUN: %clang_cc1 -std=c++amp -fsyntax-only %ampneg -verify %s

struct S {
    S(int) {};
};

int main()
{
    int a = 1;
    S foo((int) restrict(cpu) a);// expected-error {{Not support to define a volatile restrict(amp) function}}
    return 1;
}

