// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>Verify that restriction modifier keyword is contextual keyword with respect to lambda expression.</summary>

// RUN: %cxxamp %s %link
// RUN: ./a.out

using namespace std;

bool test1()
{
   typedef int restrict;

   auto x = []()mutable restrict(cpu) -> restrict { return 1;}();   

   return (x == 1);
}

class restrict : std::exception
{
    public:
    explicit restrict(const char* message)
    {
    }
};

auto lambda1 = []() restrict(cpu) throw(restrict) { throw restrict("Test");};
bool test2()
{
   try
   {
      lambda1();
      return false;
   }
   catch(restrict ex)
   {
      return true;   
   }
}


bool test3()
{
   int restrict = []() restrict(cpu) -> int{ return 1;}();

   return (restrict == 1);
}

bool test4()
{
   class restrict
   {
      public:
        int data;
   };

   restrict x, y;

   x.data = 10;
   y.data = 20;

   int  result = [&](restrict a) restrict(cpu) -> int {return x.data + a.data;}(y);

    return (result == 30);   
}

bool test5()
{
    auto result  = [](int restrict) restrict(cpu) -> int {return 10;}(10); 

    return (result == 10);
}

bool test6()
{
    int restrict  = 0;

    auto result = [restrict]() restrict(cpu) -> bool {return true;}();

    return result;
}


bool test7()
{
    int restrict  = 0;

    [&]() restrict(cpu) {restrict++;}();

    return (restrict == 1);
}

// Main entry point
int main(int argc, char **argv) 
{
    bool passed = true;

    if(test1())
    {
    }
    else
    {
        passed = false;
    }

    if(test2())
    {
    }
    else
    {
        passed = false;
    }

    if(test3())
    {
    }
    else
    {
        passed = false;      
    }

    if(test4())
    {
    }
    else
    {
        passed = false;    
    }

    if(test5())
    {
    }
    else
    {
        passed = false;     
    }

    if(test6())
    {
    }
    else
    {
        passed = false;  
    }

    if(test7())
    {
    }
    else
    {
        passed = false;      
    }

    return passed ? 0 : 1;
}
