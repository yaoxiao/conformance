// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P2</tags>
/// <summary>Use different modifiers on a declaration and definiton</summary>
//#Expects: Error: error C3935

// RUN: %clang_cc1 -std=c++amp -fsyntax-only %ampneg -verify %s

struct S
{
    int test() restrict(amp,cpu);
};

int S::test() restrict(amp) // expected-error {{Not support to use different modifiers on a declaration and definiton}}	
{
    return 1;
}

int main()
{
    return 1;
}

