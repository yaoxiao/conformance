// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P0</tags>
/// <summary>Access tile_static variable from a local class.</summary>
// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out
#include "../../../../../../device.h"
#include <amp.h>
using namespace concurrency;

int main()
{
	std::vector<int> res_(2);
	array_view<int> res(res_.size(), res_);
	parallel_for_each(extent<1>(1).tile<1>(), [=](tiled_index<1>) restrict(amp)
	{
		tile_static int ts_i;
		ts_i = 0; 
		struct obj
		{
			void f() { ts_i = 1; } // Store
			int g() { return ts_i + 1; } // Load
		};
		obj().f();
		res[0] = ts_i;
		res[1] = obj().g();
	});
	
	int result = 1;
    result &= (res[0] == 1);
    result &= (res[1] == 2);
	return !result;
}
