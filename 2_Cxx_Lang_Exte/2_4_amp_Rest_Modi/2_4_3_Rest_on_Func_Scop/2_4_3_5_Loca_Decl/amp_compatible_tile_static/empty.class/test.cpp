// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>Make sure we can create a tile_static variable of type empty class inside an amp function</summary>
// RUN: %cxxamp %s %link
// RUN: ./a.out
#include <amp.h>
#include <vector>
#include "../../../../../../device.h"
#define GPU_INVOKE(gpui_acclv, gpui_func_Tresult, gpui_func, ...) [&]() mutable { \
    gpui_func_Tresult gpui_result; \
    concurrency::array_view<gpui_func_Tresult, 1> gpui_resultv(1, &(gpui_result)); \
    gpui_resultv.discard_data(); \
    concurrency::parallel_for_each(gpui_acclv, gpui_resultv.get_extent(), [=](concurrency::index<1> gpui__idx) restrict(amp) { \
        gpui_resultv[gpui__idx] = (gpui_func)(__VA_ARGS__); \
    }); \
    return gpui_resultv[0]; \
}()
using namespace Concurrency;
using namespace Concurrency::Test;
struct A
{
    int get() restrict(cpu, amp)
    {
        return 1;
    }
};

int test() restrict(amp)
{
    tile_static A a;
    
    return a.get(); 
}

int main()
{
    accelerator_view av = require_device().get_default_view();
    
    int result = (GPU_INVOKE(av,int, test) == 1);
    
    return !result;
}
