// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>Capture a restrict-amp function pointer by value in a restrict(cpu) lambda</summary>

// RUN: %clang_cc1 -std=c++amp -fsyntax-only %ampneg -verify %s

int test() restrict(amp)
{
    return 1;
}

int main() 
{
    int (*pTest)() restrict(amp) = test;
    
    auto l = [pTest] () {};
        
    return 0;
}
// expected-error@18 {{use of undeclared identifier 'pTest'}}
// expected-error@20 {{use of undeclared identifier 'pTest'}}
//#Expects: Error: error C3939
