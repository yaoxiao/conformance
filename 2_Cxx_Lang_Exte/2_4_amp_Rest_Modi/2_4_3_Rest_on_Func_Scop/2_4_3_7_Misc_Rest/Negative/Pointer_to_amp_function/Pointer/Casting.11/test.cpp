// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P0</tags>
/// <summary>Negative: in GPU function, define class c; int c::func(int); then int (c::*pfn)(int) __GPU = (int (c::*)(int) __GPU)func; call pfn in __GPU and non __GPU context</summary>
//#Expects: Error: error C3581

// RUN: %clang_cc1 -std=c++amp -fsyntax-only %ampneg -verify %s

class c
{
public:
    int f(int) 
    {
        return 1;
    };
};

bool test() restrict(amp,cpu)
{
    int flag = 0;

    bool passed = true;

    int v = 0;

    int (c::*pfn)(int) restrict(amp,cpu) = (int (c::*)(int) restrict(amp,cpu))&c::f;

    c o;

    (o.*pfn)(v);
    if (flag != 1)
    {
        return false;
    }

    return passed;
}

int main(int argc, char **argv)
{
    test();
    return 1;
}
// expected-error@29 {{expected unqualified-id}}
// expected-error@29 {{use of undeclared identifier 'pfn'}}
// expected-error@29 {{expected '(' for function-style cast or type construction}}
// expected-error@33 {{use of undeclared identifier 'pfn'}}
