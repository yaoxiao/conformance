// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P0</tags>
/// <summary>Declares a function accepting a pointer to a restrict(cpu) function (overloaded)</summary>

// RUN: %cxxamp %s %link
// RUN: ./a.out

int amp_function(int x, int y) restrict(amp)
{
    return x + y;
}

int amp_function(int x, int y) restrict(cpu)
{
    return 2 * x + y;
}

int test(int (*p)(int, int) restrict(cpu))
{
    return p(3, 4);
}

int main()
{
    int r = test(&amp_function);
    return r == 10 ? 0 : 1;
}

