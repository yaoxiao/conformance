// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P0</tags>
/// <summary>Declares a function accepting a reference to a restrict(amp) function</summary>

// RUN: %clang_cc1 -std=c++amp -fsyntax-only %ampneg -verify %s

int amp_function(int x, int y) restrict(amp)
{
    return x + y;
}

int test(int (&p)(int, int) restrict(amp))
{
    // can't call an amp function through a pointer
    return 1;
}

int main()
{
    test(amp_function);
    return 0;
}

//#Expects: Error: error C3939
// expected-error@16 {{Declares a function accepting a reference to a restrict(amp) function}}

