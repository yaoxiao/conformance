// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>Negative: Collide restrict(cpu) function with restrict(amp) function both with extern "C" linkage</summary>

// this is negative because mangling is removed and linker will have 2 definitions of the same function

// RUN: %clang_cc1 -std=c++amp -fsyntax-only %ampneg -verify %s

extern "C" void foo() restrict(amp)
{
    
}

extern "C" void foo() restrict(cpu)
{
    
}


int main()
{
    return 0;   
}

//#Expects: Error: error C2733
 // expected-error@18 {{redefinition of 'foo'}}
 // expected-warning@18 {{attribute declaration must precede definition}}
 // expected-warning@13 {{previous definition is here}}
