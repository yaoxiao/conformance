// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P2</tags>
/// <summary>type* const p can cannot initialize type * & r in GPU code</summary>

// RUN: %clang_cc1 -std=c++amp -fsyntax-only %ampneg -verify %s

using std::vector;

void kernel() restrict(amp,cpu)
{
    float f;
    float * const p = &f;
    float * & r = p; // expected-error {{binding of reference to type 'float *' to a value of type 'float *const' drops qualifiers}}
}

//#Expects: Error: test.cpp\(19\) : error C2440

