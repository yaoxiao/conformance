// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P0</tags>
/// <summary>(NEG) Define pointers to pointer as parameter and return value</summary>
//#Expects: Error: error C3581
//#Expects: Error: error C3581
//#Expects: Error: error C3581
//#Expects: Error: error C3581
//#Expects: Error: error C3581
//#Expects: Error: error C3581
// ref bug: 226039

// RUN: %clang_cc1 -std=c++amp -fsyntax-only %ampneg -verify %s

#include <stddef.h>
using std::vector;


void f1(double **p1, float ***p2, int ****p3) restrict(amp,cpu) {} // expected-error {{Define pointers to pointer as parameter and return value}}

double ***** f2() restrict(amp,cpu) // expected-error {{Define pointers to pointer as parameter and return value}}
{
    double ***** pd = NULL; // expected-error {{Define pointers to pointer as parameter and return value}}
    return pd;
}

void f11(double **p1, float ***p2, int ****p3) restrict(amp) {} // expected-error {{Define pointers to pointer as parameter and return value}}

double ***** f22() restrict(amp) // expected-error {{Define pointers to pointer as parameter and return value}}
{
    double ***** pd = NULL; // expected-error {{Define pointers to pointer as parameter and return value}}
    return pd;
}


int main(int argc, char **argv)
{
    bool passed = false;

    return passed ? 0 : 1;
}
