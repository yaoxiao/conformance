// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P0</tags>
/// <summary>test voilatile type qualifier</summary>
//#Expects: Error: error C3581

// RUN: %clang_cc1 -std=c++amp -fsyntax-only %ampneg -verify %s

void foo() restrict(amp,cpu)
{
	volatile int a = 10; // expected-error {{Not support to use voilatile type qualifier on GPU side}}	
	volatile const int c = 400; // expected-error {{Not support to use voilatile type qualifier in amp restricted code}}
}


int main(int argc, char **argv) 
{ 
    foo();
	 [=]() restrict(amp,cpu)
    {
        const volatile int b = 300;
    }();
    return 1;
}

