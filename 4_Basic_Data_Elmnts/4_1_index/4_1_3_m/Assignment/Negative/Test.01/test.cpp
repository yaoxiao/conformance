// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED,
// INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>Tests that checks the assigning an index of a different rank fails</summary>
//#Expects: error C2679

// RUN: %clang_cc1 -std=c++amp -fsyntax-only %ampneg -verify %s

#include <amp.h>

using namespace Concurrency;

int main()
{
    const int N = 3;
    index<N> idx1(10, 11, 12);
    index<N+1> idx2;
    
    // Assign to index of different rank
    idx2 = idx1;

    return 1;
}
// expected-error@25 {{no viable overloaded '='}}
// expected-warning@25 {{candidate function not viable: no known conversion from 'index<3>' to 'const index<4>' for 1st argument}}
