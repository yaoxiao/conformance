// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED,
// INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>Check that the rank is set to N.</summary>

// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out
#include <amp.h>
#include <stdio.h>
using namespace Concurrency;
using std::vector;

int test() restrict(amp,cpu)
{
    extent<1> e1d;

    if (e1d.rank != 1)
    {
        return 11;
    }

    extent<1> e1(100);

    if (e1.rank != 1)
    {
        return 12;
    }

    extent<2> e2d;

    if (e2d.rank != 2)
    {
        return 13;
    }

    extent<2> e2(100, 200);

    if (e2.rank != 2)
    {
        return 14;
    }

    extent<3> e3d;

    if (e3d.rank != 3)
    {
        return 15;
    }

    extent<3> e3(100, 200, 300);

    if (e3.rank != 3)
    {
        return 16;
    }

    extent<4> e4d;

    if (e4d.rank != 4)
    {
        return 17;
    }

    if (e3.rank != 3)
    {
        return 18;
    }

    extent<10> e10;

    if (e10.rank != 10)
    {
        return 19;
    }

    return 0;
}

void kernel(index<1>& idx, array<int, 1>& result) restrict(amp,cpu)
{
    result[idx] = test();    
}

const int size = 10;

bool test_device()
{
#if 0
    accelerator acc;
    if (!get_device(Device::ALL_DEVICES, acc))
    {
        return 2;
    }
    accelerator_view av = acc.get_default_view();
#endif
    extent<1> e(size);
    array<int, 1> result(e);

    vector<int> presult(size, 0);    
    
    parallel_for_each(e,[&result](index<1> idx) restrict(amp,cpu){
        kernel(idx, result);
    });

    presult = result;

    for (int i = 0; i < 10; i++)
    {
        if (presult[i] != 0)
        {
            return 1;
        }
    }

    return 0;
}

int main() 
{ 
    int result = test();

    if(result != 0) return result;
    
    result = test_device();

    return result;
}


