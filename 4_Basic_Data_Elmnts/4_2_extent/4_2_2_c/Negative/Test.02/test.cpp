// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED,
// INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>(Negative) Create an extent with N < 0 and ensure that the compilation fails.</summary>
//#Expects: Error: error C2118

// RUN: %clang_cc1 -std=c++amp -fsyntax-only %ampneg -verify %s
#include <amp.h>

using namespace Concurrency;
using namespace Concurrency::Test;
using std::vector;

int test() restrict(amp,cpu)
{
    extent<-1> e1;// expected-error {{Create an extent with N < 0}}
    extent<-100> e2;// expected-error {{Create an extent with N < 0}}

    return 1;
}

void kernel(index<1>& idx, array<int, 1>& result) restrict(amp,cpu)
{
    result[idx] = test();    
}

const int size = 10;

int test_device()
{
#if 0
    accelerator acc;
    if (!get_device(Device::ALL_DEVICES, acc))
    {
        return 2;
    }
    accelerator_view av = acc.get_default_view();
#endif
    extent<1> e(size);
    array<int, 1> result(e);
    vector<int> presult(size, 0);

    parallel_for_each(e, [&result](index<1> idx) restrict(amp,cpu) {
        kernel(idx, result);
    });
    presult = result;

    for (int i = 0; i < 10; i++)
    {
        if (presult[i] != 0)
        {
            return 1;
        }
    }

    return 0;
}

int main() 
{ 
    test();
    test_device();

    //Always fail if this succeeds to compile
    return 1;
}

