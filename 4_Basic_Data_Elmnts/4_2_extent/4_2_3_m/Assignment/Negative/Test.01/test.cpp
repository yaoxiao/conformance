// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED,
// INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>(Negative) Assign an initialized extent with a different rank to this extent and ensure that compilation fails.</summary>
//#Expects: Error: test.cpp\(28\) : error C2679
//#Expects: Error: test.cpp\(33\) : error C2679
//#Expects: Error: test.cpp\(38\) : error C2679
//#Expects: Error: test.cpp\(43\) : error C2679
//#Expects: Error: test.cpp\(48\) : error C2679
//#Expects: Error: test.cpp\(53\) : error C2679

// RUN: %clang_cc1 -std=c++amp -fsyntax-only %ampneg -verify %s
#include <amp.h>
using namespace Concurrency;
using namespace Concurrency::Test;
using std::vector;

int test() restrict(cpu,amp)
{
    extent<1> e1a;
    extent<2> e2a;

    e2a = e1a;
    
    extent<1> e1b(100);
    extent<2> e2b(100, 200);

    e2b = e1b;

    extent<3> e3a;
    extent<4> e4a;

    e4a = e3a;

    extent<3> e3b(100, 200, 300);
    extent<4> e4b;

    e4b = e3b;

    extent<4> e4c;
    extent<5> e5c;

    e5c = e4c;

    extent<9> e9a;
    extent<99> e99a;

    e99a = e9a;

    return 1;
}

void kernel(index<1>& idx, array<int, 1>& result) restrict(cpu,amp)
{
    result[idx] = test();    
}

const int size = 10;

int test_device()
{
    accelerator acc;
    if (!get_device(Device::ALL_DEVICES, acc))
    {
        return 2;
    }
    accelerator_view av = acc.get_default_view();

    extent<1> e(size);
    array<int, 1> result(e, av);
    vector<int> presult(size, 0);

    parallel_for_each(e, [&](index<1> idx) restrict(cpu,amp) {
        kernel(idx, result);
    });
    presult = result;

    for (int i = 0; i < 10; i++)
    {
        if (presult[i] != 0)
        {
            return 1;
        }
    }

    return true;
}

int main() 
{ 
    int result = test();

    if(result != 0) return result;
    
    result = test_device();

    return result;
}

// expected-error@28 {{Assign an initialized extent with a different rank to this extent}}
// expected-error@33 {{Assign an initialized extent with a different rank to this extent}}

// expected-error@38 {{Assign an initialized extent with a different rank to this extent}}
// expected-error@43 {{Assign an initialized extent with a different rank to this extent}}

// expected-error@48 {{Assign an initialized extent with a different rank to this extent}}
// expected-error@53 {{Assign an initialized extent with a different rank to this extent}}
