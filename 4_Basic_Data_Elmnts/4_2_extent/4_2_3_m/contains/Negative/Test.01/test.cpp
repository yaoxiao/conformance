// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED,
// INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>Test that passing an index with a smaller rank than the extent rank to contains results in a compilation error.</summary>
//#Expects: Error: test.cpp\(23\) : error C2664
//#Expects: Error: test.cpp\(28\) : error C2664
// RUN: %clang_cc1 -std=c++amp -fsyntax-only %ampneg -verify %s
#include <amp.h>

using namespace concurrency;
using namespace concurrency::Test;

int test() restrict(cpu,amp)
{
	{
		extent<3> ext;
		index<2> idx;
		ext.contains(idx);
	}
	{
		extent<10> ext;
		index<3> idx;
		ext.contains(idx);
	}

    return 0;
}

int main(int argc, char **argv) 
{
	accelerator_view av = require_device().get_default_view();

	int result = test();
	if(result != 0) return result;

	result = GPU_INVOKE(av, int, test);
	return result;
}
// expected-error@23 {{passing an index with a smaller rank than the extent rank to contains}}
// expected-error@28 {{passing an index with a smaller rank than the extent rank to contains}}
