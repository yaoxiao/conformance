// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED,
// INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>Test const-correctness for extent and index.</summary>
// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out
#include <amp.h>
#include "../coordinates.h"
using namespace concurrency;

int test() restrict(cpu,amp)
{
	// Note: The actual result of call has minor significance here, the point is to be able to compile.

	{
		const extent<1> ext(1);
		const index<1> idx(0);
		if(!ext.contains(idx))
			return 1;
	}
	{
		extent<1> ext(1);
		const index<1> idx(0);
		if(!ext.contains(idx))
			return 1;
	}

    return 0;
}

int main()
{
	//accelerator_view av = require_device().get_default_view();

	int cpu_result = test();
	//Log() << "Test " << cpu_result << " on host\n";

	int amp_result = GPU_INVOKE(test);
	//Log() << "Test " << amp_result << " on device\n";

	return cpu_result & amp_result;
}

