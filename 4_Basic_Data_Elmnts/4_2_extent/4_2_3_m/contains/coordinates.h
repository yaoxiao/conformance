//--------------------------------------------------------------------------------------
// File: coordinates.h
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//--------------------------------------------------------------------------------------
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
// file except in compliance with the License.  You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR
// CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
//
// See the Apache Version 2.0 License for specific language governing permissions
// and limitations under the License.
//
//--------------------------------------------------------------------------------------
//

#pragma once 

#include <amp.h>
#include <iterator>
#include <memory>
static const int runall_pass   =   0;
static const int runall_fail   =   1;
static const int runall_skip   =   2;
static const int runall_cascade_fail = 3;
static const int runall_no_value     = 4;
#define GPU_INVOKE(gpui_func, ...) [&]() mutable { \
    int gpui_result; \
    concurrency::array_view<int, 1> gpui_resultv(1, &(gpui_result)); \
    gpui_resultv.discard_data(); \
    concurrency::parallel_for_each(gpui_resultv.get_extent(), [=](concurrency::index<1> gpui__idx) restrict(amp) { \
		gpui_resultv[gpui__idx] = (gpui_func)(__VA_ARGS__); \
	}); \
	return gpui_resultv[0]; \
}()

namespace Concurrency
{
namespace Test
{
    index<4> make_index(int i0, int i1, int i2, int i3) restrict(cpu,amp)
    {
        int subscripts[4];
        subscripts[0] = i0;
        subscripts[1] = i1;
        subscripts[2] = i2;
        subscripts[3] = i3;
        return index<4>(subscripts);
    }
    
    index<5> make_index(int i0, int i1, int i2, int i3, int i4) restrict(cpu,amp)
    {
        int subscripts[5];
        subscripts[0] = i0;
        subscripts[1] = i1;
        subscripts[2] = i2;
        subscripts[3] = i3;
        subscripts[4] = i4;
        return index<5>(subscripts);
    }
    
    extent<4> make_extent(int i0, int i1, int i2, int i3) restrict(cpu,amp)
    {
        int subscripts[4];
        subscripts[0] = i0;
        subscripts[1] = i1;
        subscripts[2] = i2;
        subscripts[3] = i3;
        return extent<4>(subscripts);
    }
    
    extent<5> make_extent(int i0, int i1, int i2, int i3, int i4) restrict(cpu,amp)
    {
        int subscripts[5];
        subscripts[0] = i0;
        subscripts[1] = i1;
        subscripts[2] = i2;
        subscripts[3] = i3;
        subscripts[4] = i4;
        return extent<5>(subscripts);
    }

}
}

