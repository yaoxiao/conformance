// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED,
// INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>(Negative) Check that comparing extents of incompatible ranks results in a compilation error </summary>
//#Expects: Error: test.cpp\(29\)
//#Expects: Error: test.cpp\(47\)

// RUN: %clang_cc1 -std=c++amp -fsyntax-only %ampneg -verify %s
#include <amp.h>

using namespace Concurrency;
using std::vector;

bool test_equal() restrict(amp,cpu)
{
    int flag = 0;

    int data1[] = {-10, -1, 0, 1, 10};
    int data2[] = {-10, -1, 0, 1};

    extent<5> e1(data1);
    extent<4> e2(data2);

    if (e1 == e2)//expected-error{{invalid operands to binary expression ('extent<5>' and 'extent<4>')}}
    {
        flag = 1;
    }

    return false;
}

bool test_not_equal() restrict(amp,cpu)
{
    int flag = 0;

    int data1[] = {-10, -1, 0, 1, 10};
    int data2[] = {-100, -11, 1, 11, 100, 7};

    extent<5> e1(data1);
    extent<6> e2(data2);

    if (e1 != e2)//expected-error{{invalid operands to binary expression ('extent<5>' and 'extent<6>')}}
    {
        flag = 1;
    }

    return false;
}

bool test() restrict(amp,cpu)
{
    return (test_equal() && test_not_equal());
}

void kernel(index<1>& idx, array<int, 1>& result) restrict(amp,cpu)
{
    if (!test())
    {
        result[idx] = 1;
    }
}

const int size = 10;

bool test_device()
{
#if 0
    accelerator device;
    if (!get_device(Device::ALL_DEVICES, device))
    {
        return 2;
    }
    accelerator_view av = device.get_default_view();
#endif
    extent<1> e(size);
    array<int, 1> result(e);
    vector<int> presult(size, 0);

    parallel_for_each(e, [&result](index<1> idx) restrict(amp,cpu){
        kernel(idx, result);
    });
    presult = result;

    for (int i = 0; i < 10; i++)
    {
        if (presult[i] == 1)
        {
            return false;
        }
    }

    return true;
}

int main(int argc, char **argv) 
{ 
    test();
    test_device();
    
    //Always Fail if we run to completion. this test should fail to compile
    return 1;
}

