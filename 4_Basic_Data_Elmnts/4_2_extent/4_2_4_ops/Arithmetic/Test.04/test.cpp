// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED,
// INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>checking operators explicitly.</summary>
// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out
#include <amp.h>

using namespace Concurrency;
using std::vector;

int test() restrict(amp,cpu)
{
    extent<3> ea(4, 4, 4);    
    extent<3> er, et;

    er = extent<3>(6, 6, 6);
    et = operator+(ea, 2);

    if (!(et == er))
    {
        return 11;
    }

    er = extent<3>(2, 2, 2);
    et = operator-(ea, 2);

    if (!(et == er))
    {
        return 12;
    }

    er = extent<3>(8, 8, 8);
    et = operator*(ea, 2);

    if (!(et == er))
    {
        return 13;
    }

    er = extent<3>(2, 2, 2);
    et = operator/(ea, 2);

    if (!(et == er))
    {
        return 14;
    }

	et = ea + extent<3>(1,2,3); // et <=> (5,6,7)
	er = extent<3>(1, 0, 1);
    et = operator%(et, 2);

    if (!(et == er))
    {
        return 15;
    }

    return 0;
}

void kernel(index<1>& idx, array<int, 1>& result) restrict(amp,cpu)
{
    result[idx] = test();    
}

const int size = 10;

int test_device()
{
#if 0

    accelerator acc;
    if (!get_device(Device::ALL_DEVICES, acc))
    {
        return 2;
    }
    accelerator_view av = acc.get_default_view();
#endif

    extent<1> e(size);
    array<int, 1> result(e);    

    parallel_for_each(e, [&result](index<1> idx) restrict(amp,cpu) {
        kernel(idx, result);
    });
    vector<int> presult = result;

    for (int i = 0; i < 10; i++)
    {
        if (presult[i] != 0)
        {
            return 1;
        }
    }

    return 0;
}

int main() 
{
    int result = 1;

    result &= (test() == 0 );
    result &= (test_device() ==0 );

    return !result;
}
