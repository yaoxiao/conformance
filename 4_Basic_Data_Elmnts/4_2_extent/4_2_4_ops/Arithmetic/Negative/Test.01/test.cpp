// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED,
// INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.
/// <tags>P1</tags>
/// <summary>Check that applying the operator on extents of incompatible ranks rerults in a compilation error</summary>
//#Expects: Error: test.cpp\(42\)
//#Expects: Error: test.cpp\(43\)
//#Expects: Error: test.cpp\(44\)
//#Expects: Error: test.cpp\(45\)
//#Expects: Error: test.cpp\(57\)
//#Expects: Error: test.cpp\(58\)
//#Expects: Error: test.cpp\(59\)
//#Expects: Error: test.cpp\(60\)
//#Expects: Error: test.cpp\(72\)
//#Expects: Error: test.cpp\(73\)
//#Expects: Error: test.cpp\(74\)
//#Expects: Error: test.cpp\(75\)
//#Expects: Error: test.cpp\(87\)
//#Expects: Error: test.cpp\(88\)
//#Expects: Error: test.cpp\(89\)
//#Expects: Error: test.cpp\(90\)

// RUN: %clang_cc1 -std=c++amp -fsyntax-only %ampneg -verify %s
#include <amp.h>

using namespace Concurrency;
using namespace Concurrency::Test;
using std::vector;


int test1() restrict(amp,cpu)
{
    int data[] = {200, 100, 2000, 0, -100, -10, -1, 0,  1,  10, 100};
    extent<1> e1;
    extent<2> e2;
    extent<1> er;

    er = e1 + e2;
    er = e1 - e2;
    er = e1 * e2;
    er = e1 / e2;

    return 1;
}

int test2() restrict(amp,cpu)
{
    int data[] = {200, 100, 2000, 0, -100, -10, -1, 0,  1,  10, 100};
    extent<3> e1;
    extent<4> e2;
    extent<3> er;

    er = e1 + e2;
    er = e1 - e2;
    er = e1 * e2;
    er = e1 / e2;

    return 1;
}

int test3() restrict(amp,cpu)
{
    int data[] = {200, 100, 2000, 0, -100, -10, -1, 0,  1,  10, 100};
    extent<4> e1;
    extent<5> e2;
    extent<4> er;

    er = e1 + e2;
    er = e1 - e2;
    er = e1 * e2;
    er = e1 / e2;

    return 1;
}

int test4() restrict(amp,cpu)
{
    int data[] = {200, 100, 2000, 0, -100, -10, -1, 0,  1,  10, 100};
    extent<10> e1;
    extent<11> e2;
    extent<10> er;

    er = e1 + e2;
    er = e1 - e2;
    er = e1 * e2;
    er = e1 / e2;

    return 1;
}

int test() restrict(amp,cpu)
{
    int result = test1();
    
    result = (result == 0) ? test2() : result;

    result = (result == 0) ? test3() : result;

    result = (result == 0) ? test4() : result;

    return result;
}

void kernel(index<1>& idx, array<int, 1>& result) restrict(amp,cpu)
{
    result[idx] = test();    
}

const int size = 10;

int test_device()
{
    accelerator acc;
    if (!get_device(Device::ALL_DEVICES, acc))
    {
        return 2;
    }

    accelerator_view av = acc.get_default_view();
    extent<1> e(size);
    array<int, 1> result(e, av);
    vector<int> presult(size, 0);

    parallel_for_each(e, [&](index<1> idx) restrict(amp,cpu) {
        kernel(idx, result);
    });
    presult = result;

    for (int i = 0; i < 10; i++)
    {
        if (presult[i] != 0)
        {
            return 1;
        }
    }

    return 0;
}

int main(int argc, char **argv) 
{ 
    int result = test();

    if(result != 0) return result;
    
    result = test_device();

    return result;
}
// expected-error@42 {{the operator on extents of incompatible ranks}}
// expected-error@43 {{the operator on extents of incompatible ranks}}
// expected-error@44 {{the operator on extents of incompatible ranks}}
// expected-error@45 {{the operator on extents of incompatible ranks}}

// expected-error@57 {{the operator on extents of incompatible ranks}}
// expected-error@58 {{the operator on extents of incompatible ranks}}
// expected-error@59 {{the operator on extents of incompatible ranks}}
// expected-error@60 {{the operator on extents of incompatible ranks}}

// expected-error@72 {{the operator on extents of incompatible ranks}}
// expected-error@73 {{the operator on extents of incompatible ranks}}
// expected-error@74 {{the operator on extents of incompatible ranks}}
// expected-error@75 {{the operator on extents of incompatible ranks}}

// expected-error@87 {{the operator on extents of incompatible ranks}}
// expected-error@88 {{the operator on extents of incompatible ranks}}
// expected-error@89 {{the operator on extents of incompatible ranks}}
// expected-error@90 {{the operator on extents of incompatible ranks}}

