// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.

/// <summary>Tests the assignment operator works the same as copy constructor and tests logical operators. This test assumes a default accelerator is available and is different from cpu accelerator.If assumption is invalid test will skip</summary>
// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out

#include "../../../accelerator.common.h"

using namespace Concurrency;

int main()
{
    int result = 1;

    accelerator acc ;//= Test::require_device();
    accelerator_view av = acc.get_default_view();
    accelerator_view av_assign = acc.create_view();
    accelerator_view av_copy(av);
    av_assign = av;

    // verify = operator works same as copy constr
    result &= (av_assign == av_copy);
    result &= (is_accelerator_view_equal(av_assign, av_copy));
    result &= (run_simple_p_f_e(av_assign));

    accelerator acc_cpu = accelerator(accelerator::cpu_accelerator);
    accelerator_view av_cpu = acc_cpu.get_default_view();
    accelerator_view av_assign_cpu = acc_cpu.create_view();
    av_assign_cpu = av_cpu;

    // verify = operator generate operable and equal view for cpu accelecrator
    result &= (av_assign_cpu == acc_cpu.get_default_view());
    result &= (is_accelerator_view_equal(acc_cpu.get_default_view(), av_assign_cpu));
    result &= (is_accelerator_view_operable(av_assign_cpu));

    // verify == operator
    result &= ((av_assign_cpu == acc_cpu.get_default_view()) == true);
    result &= ((av_assign_cpu == av_copy) == false);

    // verify != operator
    result &= ((av_assign_cpu != av_copy) == true);
    result &= ((av_assign_cpu != acc_cpu.get_default_view()) == false);

    return !result;
}
