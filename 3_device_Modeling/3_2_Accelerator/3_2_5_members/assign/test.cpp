// Copyright (c) Microsoft
// All rights reserved
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE, MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache Version 2.0 License for specific language governing permissions and limitations under the License.

/// <summary>Tests the assignment operator chaining and that it produce same result as copy constructor. This test assumes a default accelerator is available and is different from cpu accelerator.If assumption is invalid test will skip</summary>
// RUN: %amp_device -D__GPU__ %s -m32 -emit-llvm -c -S -O2 -o %t.ll && mkdir -p %t
// RUN: %clamp-device %t.ll %t/kernel.cl
// RUN: pushd %t && %embed_kernel kernel.cl %t/kernel.o && popd
// RUN: %cxxamp %link %t/kernel.o %s -o %t.out && %t.out

#include "../../../accelerator.common.h"
#include "../../../../device.h"

using namespace Concurrency;

int main()
{
    int result = 1;

    accelerator acc_ref = Test::require_device();
    accelerator acc1 = accelerator(accelerator::cpu_accelerator);
    accelerator acc2 = accelerator(accelerator::cpu_accelerator);
    accelerator acc3 = accelerator(accelerator::cpu_accelerator);

    acc1 = acc2 = acc3 ;

    // verify that = operator produce a copy accelerator that is operable
    result &= (acc1 == acc_ref);
    result &= (acc2 == acc_ref);
    result &= (acc3 == acc_ref);
    result &= run_simple_p_f_e(acc1.get_default_view());
    result &= run_simple_p_f_e(acc2.get_default_view());
    result &= run_simple_p_f_e(acc3.get_default_view());

    // verify that = operator have same result as copy constructor
    accelerator acc_copy(acc_ref);
    result &= (acc_copy == acc1);

    return !result;
}
